\%{?!python_module:%define python_module() python-%{**} python3-%{**}}
Name:           python-orderedmultidict
Version:        1.0
Release:        1
Summary:        Ordered Multivalue Dictionary
License:        Unlicense
Group:          Development/Languages/Python
URL:            https://github.com/gruns/orderedmultidict
Source:         https://files.pythonhosted.org/packages/source/o/orderedmultidict/orderedmultidict-%{version}.tar.gz
BuildRequires:  %{python_module setuptools}
BuildRequires:  fdupes
BuildRequires:  python-rpm-macros
Requires:       python-six >= 1.8.0
BuildArch:      noarch


BuildRequires:  %{python_module six >= 1.8.0}

%description
Ordered Multivalue Dictionary - omdict.

%prep
%setup -q -n orderedmultidict-%{version}
sed -i 's/^.*flake8.*/tests_require = []/' setup.py
chmod a-x README.md LICENSE.md
rm -r *.egg-info

%build
%python_build

%install
%python_install
%python_expand %fdupes %{buildroot}%{$python_sitelib}

%check
%pyunittest discover -v tests/

%files
%doc README.md
%license LICENSE.md
%{python_sitelib}/*

%changelog
* Fri Sep 3 2021 mrcangye <mrcangye@email.cn> - 1.0-1
- Initial release
